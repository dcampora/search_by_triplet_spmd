How to
------

First download ispc:

    https://sourceforge.net/projects/ispcmirror/files/v1.10.0/ispc-v1.10.0-linux.tar.gz/download

Then extract it, add the `/bin` folder to the path, and do the following:

```
    source /cvmfs/sft.cern.ch/lcg/releases/clang/6.0.0-6647e/x86_64-centos7-gcc62-opt/setup.sh
    mkdir build
    cd build
    cmake ..
    make
```

You will need to modify the location to point to a valid folder containing
Allen's files. Change `main.cpp` for that:

```
    std::string folder_name_raw = "/localprojects/shared/1kevents_minbias/banks/";
    std::string folder_name_detector_configuration = "/localprojects/dcampora/allen/input/detector_configuration/";
```
